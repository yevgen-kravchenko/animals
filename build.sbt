name := "animals"
version := "0.1"
organization := "npl.edu"
scalaVersion := "2.13.2"

libraryDependencies += "org.typelevel" %% "cats-core" % "2.10.0"
