package edu.npl

import cats.syntax.option._

case class Question(question: String, answer: String, mutation: State => State, predicate: State => Boolean)

object Question {

  val redColour = Question("Is animal colour brown? (y/n)",
    "y",
    s => s.withColour(Colour.Red.some),
    s => s.colour.isEmpty)
  val brownColour = Question("Is animal colour red? (y/n)",
    "y",
    s => s.withColour(Colour.Brown.some),
    s => s.colour.isEmpty)
  val spotsPattern = Question("Animal has spots pattern (y/n)",
    "y",
    s => s.withPattern(Pattern.Spots.some),
    s => s.pattern.isEmpty)
  val stripesPattern = Question("Animal has stripes pattern (y/n)",
    "y",
    s => s.withPattern(Pattern.Stripes.some),
    s => s.pattern.isEmpty)
  val hasHair = Question("Has animal hair? (y/n)",
    "y",
    s => s.withProperty(TypeProperty.HasHair.some),
    s => s.property.isEmpty)
  val milk = Question("Does animal give milk? (y/n)",
    "y",
    s => s.withProperty(TypeProperty.Milk.some),
    s => s.property.isEmpty)
  val meat = Question("Does animal eat meat? (y/n)",
    "y",
    s => s.withProperty(TypeProperty.EatsMeat.some),
    s => s.property.isEmpty)
  val teeth = Question("Has animal sharp teeth? (y/n)",
    "y",
    s => s.withProperty(TypeProperty.SharpTeeth.some),
    s => s.property.isEmpty)
  val hooves = Question("Has animal hooves? (y/n)",
    "y",
    s => s.withProperty(TypeProperty.HasHooves.some),
    s => s.property.isEmpty)

  val questions = List(
    redColour,
    brownColour,
    spotsPattern,
    stripesPattern,
    hasHair,
    milk,
    meat,
    teeth,
    hooves,
  )
}
