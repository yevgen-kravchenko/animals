package edu.npl


trait Colour

object Colour {
  case object Red extends Colour
  case object Brown extends Colour
}

trait Pattern

object Pattern {
  case object Spots extends Pattern
  case object Stripes extends Pattern
}

trait Type

object Type {
  case object Mammal extends Type
  case object Carnivor extends Type
  case object Ungulate extends Type
}

trait TypeProperty

object TypeProperty {
  object HasHair extends TypeProperty
  object Milk extends TypeProperty
  object EatsMeat extends TypeProperty
  object SharpTeeth extends TypeProperty
  object HasHooves extends TypeProperty
  object ChewsCud extends TypeProperty
}

abstract class Animal(val name: String)

object Animal {
  case object Zebra extends Animal("Zebra")
  case object Monkey extends Animal("Monkey")
  case object Giraffe extends Animal("Giraffe")
  case object Tiger extends Animal("Tiger")
}

