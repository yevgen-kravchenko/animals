package edu.npl

case class State(animal: Option[Animal],
                 colour: Option[Colour],
                 pattern: Option[Pattern],
                 animalType: Option[Type],
                 property: Option[TypeProperty]) {


  def withProperty(p: Option[TypeProperty]): State = {
    val newType = Rules.deriveType(p)
    val newAnimal = Rules.deriveAnimal(this.colour, this.pattern, newType)
    this.copy(animalType = newType, animal = newAnimal)
  }

  def withColour(c: Option[Colour]): State = {
    val newAnimal = Rules.deriveAnimal(c, this.pattern, this.animalType)
    this.copy(colour = c, animal = newAnimal)
  }

  def withPattern(p: Option[Pattern]): State = {
    val newAnimal = Rules.deriveAnimal(this.colour, p, this.animalType)
    this.copy(pattern = p, animal = newAnimal)
  }

  def isFinal: Boolean = this.animal.isDefined

  def result: String = this.animal match {
    case Some(a) => a.name
    case None => ""
  }
}

object State {
  val empty: State = State(None, None, None, None, None)
}