package edu.npl

import scala.annotation.tailrec
import scala.io.StdIn.readLine
import scala.util.Random

object System {

  def main(args: Array[String]): Unit = {
    runLoop(Random.shuffle(Question.questions), State.empty)
  }

  @tailrec
  def runLoop(questions: List[Question], state: State): Unit = {
    if (state.isFinal) {
      println(s"Your animal is ${state.result}")
      return
    }
    if (questions.isEmpty && !state.isFinal) {
      println("Unfortunately couldn't find animal")
      return
    }

    val question = questions.head
    println(question.question)

    val answer = readLine()
    val updatedState = if (answer == question.answer) question.mutation(state) else state
    val updatedQuestions = questions.drop(1).filter(q => q.predicate(updatedState))

    runLoop(updatedQuestions, updatedState)
  }
}
