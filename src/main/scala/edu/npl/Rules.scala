package edu.npl

object Rules {

  def deriveType(p: Option[TypeProperty]): Option[Type] = p match {
    case Some(TypeProperty.HasHair) | Some(TypeProperty.Milk) => Some(Type.Mammal)
    case Some(TypeProperty.EatsMeat) | Some(TypeProperty.SharpTeeth) => Some(Type.Carnivor)
    case Some(TypeProperty.HasHooves) | Some(TypeProperty.ChewsCud) => Some(Type.Ungulate)
    case _ => None
  }

  def deriveAnimal(c: Option[Colour], p: Option[Pattern], t: Option[Type]): Option[Animal] = (c, p, t) match {
    case (Some(Colour.Red) | Some(Colour.Brown), Some(Pattern.Spots), Some(Type.Mammal)) => Some(Animal.Monkey)
    case (None, Some(Pattern.Stripes), Some(Type.Carnivor)) => Some(Animal.Tiger)
    case (None, None, Some(Type.Ungulate)) => Some(Animal.Giraffe)
    case (None, Some(Pattern.Stripes), Some(Type.Ungulate)) => Some(Animal.Zebra)
    case _ => None
  }
}
